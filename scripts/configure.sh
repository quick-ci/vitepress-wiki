#!/bin/sh

if [ ! -f "${WIKI_SOURCE_DIR}/package.json" ]; then
    wget -O ${WIKI_SOURCE_DIR}/package.json https://gitlab.com/quick-ci/vitepress-wiki/-/raw/main/package.json
fi

if [ ! -d "${WIKI_SOURCE_DIR}/.vitepress" ]; then
    mkdir ${WIKI_SOURCE_DIR}/.vitepress
fi

if [ ! -f "${WIKI_SOURCE_DIR}/.vitepress/config.js" ]; then
    wget -O wiki_default_conf.yml https://gitlab.com/quick-ci/vitepress-wiki/-/raw/main/defaults/config.js
    envsubst < wiki_default_conf.yml > ${WIKI_SOURCE_DIR}/.vitepress/config.js
    rm wiki_default_conf.yml
fi