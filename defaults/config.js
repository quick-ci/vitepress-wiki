export default {
    title: '${CI_PROJECT_NAME}',
    base: '/${CI_PROJECT_NAME}/',
    outDir: '../public',
    themeConfig: {
        nav: [
            { text: 'Home', link: '/'},
            { text: 'Usage', link: '/'}
        ]
    }
}